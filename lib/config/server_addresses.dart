class ServerAddresses {
  //Server address of your project should go here
  static const serverAddress = 'https://xo.kz';
  static const serverAddressAPI = 'http://206.81.29.96';
  // ?consumer_key=ck_*****&consumer_secret=cs_**** goes here
  static const _woocommerceKeys = '?consumer_key=ck_37db5c19372e4bf7f6a450aadea71fdaab9af492&consumer_secret=cs_212a3117fd0299fa80e80b7faaeb0cead0e9973e';
  static const _categorySuffix = '/api/categories/'; //id
  static const _productSuffix = '/api/products/';
  static const _promoSuffix = '/wp-json/wc/v3/reports/coupons/';
  static const signUp = ''; // TODO need an endpoint for this
  static const forgotPassword = ''; // TODO need an endpoint for this

  /// For more information about wp-rest-api plugin
  /// https://wordpress.org/plugins/jwt-authentication-for-wp-rest-api/
  static const authToken = '/api/auth/login';

  static const checkout = '/api/orders';

  //CACHED API (for test purposes only)
  static const _productCategoriesCached = '/cachedapi/v3/products/categories.json';
  static const _productsCached = '/cachedapi/v3/products/products.json';
  static const _promosCached = '/cachedapi/v3/coupon.json';

  static bool useStatic = _woocommerceKeys.isEmpty;

  static String get productCategories => 
    (useStatic ? _productCategoriesCached 
      : serverAddressAPI + _categorySuffix);

  static String get products => 
    (useStatic ? _productsCached 
      : serverAddressAPI + _productSuffix);

  static String get promos => 
    (useStatic ? _promosCached 
      : serverAddress  + _promoSuffix + _woocommerceKeys);
}
