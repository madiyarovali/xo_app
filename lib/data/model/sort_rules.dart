class SortRules {
  final SortOrder sortOrder;
  final SortType sortType;

  String get jsonRuleName {
    switch (sortType) {
      case SortType.DateAdded:
        return 'date';
      case SortType.Reviewed:
        return 'date'; //TODO change something about it
      case SortType.Price:
        return 'date'; //TODO change something about it
      case SortType.Popularity:
      default:
        return 'date'; //TODO change something about it
    }
  }

  const SortRules(
      {this.sortOrder = SortOrder.FromHighestToLowest,
      this.sortType = SortType.DateAdded});

  String get jsonOrder =>
      sortOrder == SortOrder.FromLowestToHighest ? 'asc' : 'desc';

  static List<SortRules> get options => SortType.values.fold(
      [],
      (list, sortType) => list
        ..addAll([
          SortRules(
              sortType: sortType, sortOrder: SortOrder.FromHighestToLowest),
          SortRules(
              sortType: sortType, sortOrder: SortOrder.FromLowestToHighest),
        ]));

  String getSortTitle() {
    switch (sortType) {
      case SortType.DateAdded:
        if (sortOrder == SortOrder.FromHighestToLowest) {
          return 'Сначала новые';
        } else {
          return 'Сначала старые';
        }
        break;
      case SortType.Reviewed:
        if (sortOrder == SortOrder.FromHighestToLowest) {
          return 'Сначала обсуждаемые';
        } else {
          return 'Сначала необсуждаемые';
        }
        break;
      case SortType.Price:
        if (sortOrder == SortOrder.FromHighestToLowest) {
          return 'Сначала дорогие';
        } else {
          return 'Сначала дешевые';
        }
        break;
      case SortType.Popularity:
        if (sortOrder == SortOrder.FromHighestToLowest) {
          return 'Сначала популярные';
        } else {
          return 'Сначала непопулярные';
        }
        break;
      default:
        return 'Нет сортировки';
    }
  }

  Map<SortType, String> get sortTextVariants => {
        SortType.DateAdded: 'По дате добавления',
        SortType.Reviewed: 'По отзывам',
        SortType.Price: 'По цене',
        SortType.Popularity: 'По популярности'
      };

  SortRules copyWithChangedOrder() {
    return SortRules(
        sortType: sortType,
        sortOrder: sortOrder == SortOrder.FromHighestToLowest
            ? SortOrder.FromLowestToHighest
            : SortOrder.FromHighestToLowest);
  }
}

enum SortType { DateAdded, Reviewed, Price, Popularity }
enum SortOrder { FromLowestToHighest, FromHighestToLowest }
