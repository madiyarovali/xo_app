/*
 * @author Martin Appelmann <exlo89@gmail.com>
 * @copyright 2020 Open E-commerce App
 * @see user_repository.dart
 */

import 'package:flutter/material.dart';
import 'package:openflutterecommerce/data/model/payment_method.dart';
import 'package:openflutterecommerce/data/model/shipping_address.dart';

abstract class CheckoutRepository {
  Future<String> confirm({
    @required List cartItems,
    @required PaymentMethodModel paymentMethod,
    @required ShippingAddressModel shippingAddress
  });
}
