/*
 * @author Andrew Poteryahin <openflutterproject@gmail.com>
 * @copyright 2020 Open E-commerce App
 * @see user_repository_impl.dart
 */

import 'package:flutter/material.dart';
import 'package:openflutterecommerce/data/model/payment_method.dart';
import 'package:openflutterecommerce/data/model/shipping_address.dart';
import 'package:openflutterecommerce/data/repositories/abstract/checkout_repository.dart';
import 'package:openflutterecommerce/data/woocommerce/repositories/checkout_remote_repository.dart';

class CheckoutRepositoryImpl extends CheckoutRepository {
  final CheckoutRemoteRepository checkoutRemoteRepository;

  CheckoutRepositoryImpl({@required this.checkoutRemoteRepository});

  @override
  Future<String> confirm({
    @required List cartItems,
    @required PaymentMethodModel paymentMethod,
    @required ShippingAddressModel shippingAddress
  }) async {
    return checkoutRemoteRepository.confirm(cartItems: cartItems, paymentMethod: paymentMethod, shippingAddress: shippingAddress);
  }
}
