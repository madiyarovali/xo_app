/*
 * @author Martin Appelmann <exlo89@gmail.com>
 * @copyright 2020 Open E-commerce App
 * @see user_repository.dart
 */

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:openflutterecommerce/config/server_addresses.dart';
import 'package:openflutterecommerce/data/model/payment_method.dart';
import 'package:openflutterecommerce/data/model/shipping_address.dart';
import 'package:openflutterecommerce/data/repositories/abstract/checkout_repository.dart';

import '../utils.dart';

class CheckoutRemoteRepository extends CheckoutRepository {
  @override
  Future<String> confirm({
    @required PaymentMethodModel paymentMethod,
    @required ShippingAddressModel shippingAddress,
    @required List cartItems,
  }) async {
    var route = HttpClient().createUri(ServerAddresses.checkout);
    var data = <String, dynamic>{
      'items': cartItems,
      'payment_method': paymentMethod,
      'shipping_address': shippingAddress
    };

    var response = await http.post(route, body: data);
    Map jsonResponse = json.decode(response.body);
    if (response.statusCode != 200) {
      throw jsonResponse['message'];
    }
    return jsonResponse['access_token'];
  }
}
